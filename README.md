### Dependencies

Hugo 0.5 extended

### Get started

To start the local server, under CMD, run `hugo server --disableFastRender --port 3002`

Open http://localhost:3002/blender/

To build the assets run `hugo`

### User events

##### Category

onclick="track(this, 'category', 'action', 'name'); "

onclick="track(this, '{{. Params.model}} contact', 'initiate contact ', 'name'); "

* navbar
   - navigate to MENU
   - download model

* hero
   - download model
   - see guide

* features
   - FEATURE expanded

* divider
   - download model
   - see guide

* examples
   - select EXAMPLE

* user guide
   - view section

* changelog
   - expand VERSION 

* recommended assets
   - view ASSET 

* feedback
   - submit feedback 

* contact
   - initiate contact 

* version check
   - download model
