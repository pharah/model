---
title: "version 1.3.1"
draft: false
weight: 995
version: "1.3.1"
releaseDate: "05 MAR 2022"
---

**Bug fixes:**

* Fixed missing textures for eyebrows and eyelashes
