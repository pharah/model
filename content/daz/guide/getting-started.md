---
title: "Getting started"
date: "2017-11-12T00:00:00Z"
draft: false
weight: 1
slug: "getting-started"
thumb: true
---

Download and install the model just like any other DAZ model. Afterwards, it will be available under the path `People\Genesis 8 Female\Characters\PBG\Pharah`

The simplest way to add the model to your scene is to double click the `Pharah` set (sub-scene). 

Alternatively, you can add it item by item:

* add a genesis 8 Female and select it
* apply the face morph `Pharah head apply` and optionally, the body `Pharah body apply`
* apply the materials `Pharah skin`
* add the fiber mesh eyebrows, to give it more details `Pharah eyebrows (fiber)`
* if the base model you used does not have eyelashes, you can add the ones that come with the Pharah model
