---
title: "Textures source files"
date: "2017-11-12T00:00:00Z"
draft: false
weight: 4
slug: "textures"
thumb: true
---

For those familiar with Photoshop, the texture source files (PSDs) are available for [download here](./textures).

You can use them to add different makeup, tattoos and whatever else might come to your mind. Afterwards simply export the textures and back in DAZ select the body and go to the **Surfaces** tab. There, select the material you want to modify (such as torso, face, lips, etc) and change the **Base color** and **Translucency color** textures.

