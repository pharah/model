---
title: "Genitalia"
date: "2017-11-12T00:00:00Z"
draft: false
weight: 3
slug: "genitalia"
thumb: true
---

The model comes with textures for the default Genesis 8 Female (G8F) genitalia. Simple select the genitalia in the **Scene** tab, and then double click the `Pharah genitalia` materials in the **Content Library**.

When using tanlines for the body you will need to apply the `Pharah genitalia - tanlines` material as well for the genitalia.
