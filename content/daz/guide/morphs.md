---
title: "Using morphs"
date: "2017-11-12T00:00:00Z"
draft: false
weight: 2
slug: "morphs"
thumb: true
---

The model comes with a small collection of morphs for the face and hairdo.

The face ones are meant to offer more accurate (closer to the in-game model) facial expressions, seeing how a lot of the generic DAZ morphs end up looking weird at best and uncanny at worst.

The hairdo ones are meant to help with posing, and offer options such as sway left, forward and so on.

To make use of the facial expressions simply search for `Pharah` and you will find them.
