---
title: Pharah for Genesis 8 Female
description: Free 3D model of Pharah from Overwatch for Daz 3D (Genesis 8 Female)
model: daz
download: https://mega.nz/folder/GipEDaQZ#DUEsDdqHUbqZYN0OmCn5Ew
updateDate: 23 MAR
version: 1.3.2
supportedVersions: ["1.3.2", "1.3.1", "1.3"]
themeColor: "#8ea35a"
---
