---
title: "Import outfits into blender"
date: "2017-10-21T00:00:00Z"
draft: false
weight: 30
slug: "export-daz-content"
thumb: false
---

How to import outfits from DAZ Studio into Blender and add it to the Pharah model.
<br/>
<br/>

<div class="video negative-margin">
   <div style="padding:42.25% 0 0 0;position:relative;">
      <iframe src="https://player.vimeo.com/video/717082434?h=e8dabe3248&amp;badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen style="position:absolute;top:0;left:0;width:100%;height:100%;" title="Import DAZ outfits to Pharah v5 using Diffeomorphic"></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>
   </div>
</div>


 * **00:00** Add DAZ character and outfit
 * **00:34** Export the DAZ scene
 * **00:50** Create a temp blender file and import the DAZ scene
 * **01:05** Merge the rigs and convert it to MHX rig
 * **01:23** Delete everything except the outfit and copy to clipboard
 * **01:32** In the Pharah .blend file reset the rig and bodymorph
 * **01:48** Paste the outfit and transfer shapekeys
 * **02:26** Adjust the rig, solidify and subsurface modifiers
 * **03:14** Add shapekey to fix the clipping (the end result is not perfect but it illustrates the point)
 * **04:27** Preview of the final result

<br/>
<br/>



{{< step text="1. Create a new scene in DAZ Studio and add a character" >}} 
   In DAZ Studio go to **File → New**. Go to the **Smart Content** tab and from **Figures** add a Genesis 8 Female figure <span class="tooltip" data-tooltip-content="#tooltip-daz-female">such as one of these</span> to the scene.
{{< /step >}}
{{< step text="2. Locate and add the outfit you downloaded" >}} 
   For the sake of the demo I will use the {{< link href="https://www.renderosity.com/rr/mod/bcs/yoga-clothing-for-genesis-8-female-/139453" >}}Yoga Clothing for Genesis 8 Female{{< /link >}} outfit.
   <br/>
   <br/>
   The Pharah v5 model is based on the Genesis 8 female body, but outfits for older versions such as Genesis 3 work as well. That being said, the outfits should fit the body inside Daz Studio. If <span class="tooltip" data-tooltip-content="#tooltip-broken-daz-outfit">it looks broken</span> there (such as when trying some Genesis 2 outfits), then it will be broken in Blender as well, so don't bother importing it and try a different outfit.
   <br/>
   <br/>
   Download, extract and copy the content to your library folder like in step 2.1 
   <br/>
   <br/>
   In Daz Studio, from the **Content** panel on the left side, navigate to your outfit. This part can be a bit tricky, especially if you end up having many outfits. A good way to find the path of the outfit is by finding the **.duf** files inside the temp folder where you extracted the content.
   <br/>
   <br/>
   For example, in the temp folder where I extracted the yoga outfit I located the **.duf** files in
   <br/>
   `\temp\People\Genesis 8 Female\Clothing\RainbowLight\Yoga(G8F)`
   <br/>
   <br/>
   Knowing that, I can find the outfit inside DazStudio's **Content panel** following the path
   <br/>
   `MyDazLibrary\People\Genesis 8 Female\ClothingRainbowLight\Yoga(G8F)`
   <br/>
   <br/>
   <span class="tooltip" data-tooltip-content="#tooltip-daz-outfits">Having located the outfit</span>, it's time to add it to the scene. 
   <br/>
   <br/>
   Select **Genesis 8 Female** in the Scene panel on the right side and next double click the outfit items you want to add (in my case Pants and then Top). The outfits will then show up below **Genesis 8 Female** in the Scene panel (otherwise, move them inside **Genesis 8 Female** manually, in the Scene panel).  
   <br/>
   You should end up with <span class="tooltip" data-tooltip-content="#tooltip-daz-outfits-2">something like this</span>.
{{< /step >}}
{{< step text="3. Add materials to the outfit" >}} 
   In the Scene panel in the top right side, <span class="tooltip" data-tooltip-content="#tooltip-daz-select-top">select each outfit item</span> to add the materials for it.
   <br/>
   <br/>
   With the outfit item selected, in the **Content Library** panel on the left side navigate to the Materials folder and <span class="tooltip" data-tooltip-content="#tooltip-daz-top-material">double click the materials you want</span>.
{{< /step >}}
{{< step text="4. Export the scene from DAZ" >}} 
   Once you're happy with the outfits you have, it's time to export them. Go to **File → Save** to save the scene and leave the **Scene Save Options** as they are. You should remember the folder path where the file was saved.
   <br/>
   <br/>
   Go to **File → Export to Blender** to save the additional file needed (make sure to leave the file name unchanged). 
{{< /step >}}
{{< step text="5. Import the scene in Blender" >}} 
   Create a new file in blender, go to the **Daz importer** panel and press the **Import DAZ** button. Navigate to where you saved the DAZ files in the previous step and select the .duf file you saved.
      <br/>
      <br/>
      You should end up with <span class="tooltip" data-tooltip-content="#tooltip-blender-import">something like this</span>.
{{< /step >}}
{{< step text="6. Merge the armatures and convert them to a MHX rig" >}} 
   In the **Outliner** open the Filter dropdown menu and disable "Object contents", "Object children", "Meshes" and everything else below <span class="tooltip" data-tooltip-content="#tooltip-blender-armatures">apart from Armatures</span>. This will display only the Armatures in the Outliner.
   <br/>
   <br/>
   Next up, select all the armatures so that the **Genesis 8 Female** armature is both <span class="tooltip" data-tooltip-content="#tooltip-blender-armatures-3">selected and active</span>.
   <br/>
   <br/>
   Next, merge the selected armatures by going to **DAZ Importer panel → Setup → Corrections → Merge rigs**. Disable the **Create duplicate bones** checkbox in the next step and click OK.
   You should end up with only one  armature visible in the Outliner: Genesis 8 Female. 
   <br/>
   <br/>
   With that selected, go to **DAZ Importer panel → Setup → Rigging → Convert to MHX** and check <span class="tooltip" data-tooltip-content="#tooltip-blender-armatures-4">Finger IK</span> in the next step.
{{< /step >}}
{{< step text="7. Copy the outfits to Pharah blend file" >}} 
   In the Outliner, open the Filter dropdown menu again and <span class="tooltip" data-tooltip-content="#tooltip-blender-meshes">enable the meshes as well</span>. 
   <br/>
   <br/>
   In the viewport select the outfits, hit `Ctrl/Cmd + I` to invert the selection and press `Del` to delete everything else so that you end up <span class="tooltip" data-tooltip-content="#tooltip-blender-meshes-2">only with the outfit items</span> in the scene. 
   <br/>
   <br/>
   Lastly, select the outfit items again and hit  `Ctrl/Cmd + C` to copy the outfits to the clipboard.
{{< /step >}}
{{< step text="8. Reset the body and rig" >}} 
   In a separate Blender window open the Pharah v5 .blend file. 
   <br/>
   <br/>
   Select the PHARAH.rig and from the Properties panel set the skeleton in the **Rest position**. From the **Pharah v5** panel go to **Bodymorph** subpanel and click the **None** button to set all the bodymorphs to zero. The result should <span class="tooltip" data-tooltip-content="#tooltip-blender-reset">look like this</span>.
{{< /step >}}
{{< step text="9. Paste the outfit" >}} 
   Hit  `Ctrl/Cmd + V` to Paste the outfits. It should fit over the Pharah body <span class="tooltip" data-tooltip-content="#tooltip-blender-fitting">almost perfectly</span>.
{{< /step >}}
{{< step text="10. Transfer the shapekeys" >}} 
   Deselect everything. Select one outfit item and then `Shift` select the Pharah body. To transfer the shapekeys from Pharah body to the outfits to go **DAZ Importer panel → Setup → Morphs → Transfer shapekeys**. In the following step leave the settings as they are and click OK.
{{< /step >}}
{{< step text="11. Rig the outfit" >}} 
   Select the outfit item and go to Modifiers and <span class="tooltip" data-tooltip-content="#tooltip-blender-modifier-rig">set PHARAH.rig as the armature</span>.
{{< /step >}}
{{< step text="12. Repeat the process for each clothing item" >}} 
   Repeat steps #10 and #11 for each cloething item, transferring the shapekeys and setting the rig for each of them.
{{< /step >}}
{{< step text="13. Add solidify and subdivision modifiers" >}} 
   For each clothing item repeat the following steps:
 * Add a Solidify modifier with the values:
   * Offset: 1.0
   * Thickness: 0.001m
 * Below that add a Subdivision Surface modifier and leave it with the default values.
 * Select the Pharah body and from the Modifiers tab <span class="tooltip" data-tooltip-content="#tooltip-blender-copy-driver">right-click the Realtime button</span> of the Subsurface modifier and press **Copy driver**
 * Select the outfit item and <span class="tooltip" data-tooltip-content="#tooltip-blender-paste-driver">right-click the Realtime button</span> of the Subsurface modifier and press **Paste driver**

<br/>
Now you will be able to toggle the Subdivision modifiers of the Pharah model with one click, from <b>Pharah v5 panel → Settings and help → Subdivision surface</b> 
{{< /step >}}
{{< step text="14. Reset the bodymorph" >}} 
   Reset the bodymorphs of the Pharah model by clicking <b>Pharah v5 panel → Bodymorph → Reset defaults</b>.
{{< /step >}}
{{< step text="15. Add corrective shapekeys to the outfit" >}} 
   You might have some minor clipping still remaining and the simplest way to address it is to add a new shapekey for each item and edit it so you can <span class="tooltip" data-tooltip-content="#tooltip-blender-shapekeys">fix the clipping</span>.
{{< /step >}}


<br/>
<br/>

A visual summary of the importing and fitting process will look like this:

<br/>
<br/>

<figure>
   <a href="/img/blender/import-progress.jpg" target="_blank">
      <img src="/img/blender/import-progress.jpg" alt="Daz Blender import process" style="width:100%">
   </a>
  <figcaption class="text-center" style="padding-top: 8px"><b>01:</b> Imported outfit &nbsp;&nbsp; <b>02:</b> With solidify modifier &nbsp;&nbsp; <b>03:</b> Transfered shapekeys  &nbsp;&nbsp; <b>04:</b> Corrective shapekey &nbsp;&nbsp; <b>05:</b> Final result</figcaption>
</figure>











<br/>
<br/>



