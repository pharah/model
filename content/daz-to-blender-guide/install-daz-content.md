---
title: "Install Daz content"
date: "2017-10-21T00:00:00Z"
draft: false
weight: 20
slug: "install-daz-content"
thumb: false
---
Here we will cover adding content (such as outfits) to DAZ Studio so it can then me imported to Blender.
<br/>
<br/>
{{< step text="1. Download the content" >}} 
   When purchasing such items directly from the Daz Shop, they will be installed via DazCentral, however we will cover the other case: acquiring the content from different websites (such as {{< link href="https://www.renderosity.com/" >}}Renderosity{{< /link >}}).
{{< /step >}}
{{< step text="2. Copy the content to your library" >}} 
   Different items will be bundled in different ways, so the best way to process them is to extract the content in a temp folder, and then copy the relevant folders  into your library. The relevant folders vary from package to package, but usually consist of:
   - `Data`
   - `People`
   - `Runtime`
   - `Scenes`

   <br/>

   Basically navigate into your temp folder until you get these ones, and then simply copy-paste them to your library folder so that you end up with the following structure:

   - `\MyDazLibrary\Data`
   - `\MyDazLibrary\People`
   - `\MyDazLibrary\Runtime`
   - `\MyDazLibrary\Scenes`

   <br/>

   You might need to restart the Daz Studio for the changes to take effect, or right click your Library folder inside Daz Studio and click **Refresh**. 
{{< /step >}}





