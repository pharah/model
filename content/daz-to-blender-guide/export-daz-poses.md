---
title: "Import poses into blender"
date: "2017-10-21T00:00:00Z"
draft: false
weight: 40
slug: "export-daz-poses"
thumb: false
---

How to import and apply poses from DAZ Studio.
<br/>
<br/>

<div class="video negative-margin">
   <div style="padding:42.25% 0 0 0;position:relative;">
      <iframe src="https://player.vimeo.com/video/730624512?h=e8dabe3248&amp;badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen style="position:absolute;top:0;left:0;width:100%;height:100%;" title="Import DAZ poses to Pharah v5 using Diffeomorphic"></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>
   </div>
</div>

 * **00:00** Export G8F model from DAZ Studio
 * **00:25** Import G8F model to Blender
 * **00:30** Convert rig to MHX
 * **00:37** Locate the poses folder
 * **01:03** Import pose
 * **01:13** Convert rig from FK → IK
 * **01:23** Show rig extra layers
 * **01:28** Copy pose
 * **01:32** Paste pose

<br/>
<br/>

{{< step text="1. Install DAZ Poses" >}} 
   Follow the steps in section 01 and 02 to install DAZ poses.
   <br/>
   **NB:** Make sure to install **Genesis 8** poses as Genesis 3 ones will not work properly during importing to Blender.
{{< /step >}}
{{< step text="2. Locate the poses folder" >}} 
   Open up the DAZ 3D Studio and in the **Content library** tab on the left side locate the poses folder:
   
   1. You could search for `pose` and from the search results, right click one and choose <span class="tooltip" data-tooltip-content="#tooltip-daz-search-pose">Show assets in → Mapped folder</span> 
   2. Alternatively, you should be able to find it directly from `MyDazLibrary\People\Genesis 8 Female\Poses` where `MyDazLibrary` is the folder you created during step 1.2 

   Once you found the **Poses** folder inside DAZ Studio right click it and choose **Browse to folder location...** to find the full path on your hard drive. 
   <br/>
   It should be something like `C:\...\People\Genesis 8 Female\Poses`
   <br/>
   <br/>
   Save that path for later use. 
{{< /step >}}
{{< step text="3. Create a new, temp, Blender file" >}} 
   We will use this file to import the poses, process them and then copy-paste them further to the Pharah model.
{{< /step >}}
{{< step text="4. Import a Genesis 8 Female model" >}} 
   Follow the steps 3.1 — 3.5 to import a Genesis 8 Female model into this new Blender scene. 
{{< /step >}}
{{< step text="5. Convert the rig to MHX" >}} 
   Follow the steps from the 3.6 section to convert the rig to MHX.
   <br/>
   <br/>
   By this point you s should have a file with a <span class="tooltip" data-tooltip-content="#tooltip-blender-model-with-rig">G8F model and an MHX rig</span>. You should save the file for future use.
{{< /step >}}
{{< step text="6. Import a pose" >}} 
   From the **DAZ Importer** panel choose <span class="tooltip" data-tooltip-content="#tooltip-daz-importer-pose">Posing → Import pose</span> and navigate to the **Poses** folder from step 4.2 above, then select a post to import.
{{< /step >}}
{{< step text="7. Convert the the rig controls from FK → IK" >}} 
   From the MHX tab <span class="tooltip" data-tooltip-content="#tooltip-fk-ik">go to the FK/IK Switch </span>
   <br/>
   <br/>

   Under **Snap Arm Bones** press:

   - IK Arm: Snap Left
   - IK Arm: Snap right

   <br/>

   Under **Snap Leg Bones** press:

   - IK Leg: Snap Left
   - IK Leg: Snap right
{{< /step >}}
{{< step text="8. Copy-paste the pose to the Pharah model" >}} 
   First off we will need to make visible some of the extra buttons. From the MHX tab go the **Layers** section and <span class="tooltip" data-tooltip-content="#tooltip-extra-bones">enable the following options</span>:

   - Head
   - Face
   - Fingers (left)
   - Fingers (right)
   - Toes (left)
   - Toes (right)

   <br/>

   Next up, select the rig and go into **Pose Mode**, press `Ctrl + A` to select all the bones and then `Ctrl + C` to <span class="tooltip" data-tooltip-content="#tooltip-copy-pose">copy the pose</span>.

   <br/>
   <br/>
   Open up a separate instance of Blender with the Pharah model, go into **Pose Mode** and press `Ctrl + V` to <span class="tooltip" data-tooltip-content="#tooltip-paste-pose">paste the pose</span>.
{{< /step >}}
{{< step text="9. Add the pose to the Pose Library" >}} 
   By now you're done, but as a bonus you can save this pose to the **Pose Library**.
   <br/>
   <br/>
   Select the rig, go into **Pose Mode**, press `Ctrl + A` to select all the bones.
   <br/>
   <br/>
   From the **Object Data Properties** tab go to the **Pose Library** section and <span class="tooltip" data-tooltip-content="#tooltip-add-pose">press the Add Pose button</span>. 
   <br/>
   <br/>
   To apply a saved pose follow the same procedure to select all the bones and then <span class="tooltip" data-tooltip-content="#tooltip-apply-pose">press the Apply Pose Library Pose button</span>.
{{< /step >}}


<br/>
<br/>

A visual summary of the importing will look like this:

<br/>
<br/>

<figure>
   <a href="/img/blender/import-pose-progress.jpg" target="_blank">
      <img src="/img/blender/import-pose-progress.jpg" alt="Daz Blender import pose process" style="width:100%">
   </a>
   <figcaption class="text-center" style="padding-top: 8px"><b>01:</b> Model with MHX rig in rested pose &nbsp;&nbsp; <b>02:</b> Imported pose &nbsp;&nbsp; <b>03:</b> FK → IK switch  &nbsp;&nbsp; <b>04:</b> Copy-pasted pose</figcaption>
</figure>




<br/>
<br/>



