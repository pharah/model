---
title: "The setup"
date: "2017-10-21T00:00:00Z"
draft: false
weight: 10
slug: "daz-setup"
thumb: false
---

How to install the DAZ Studio and the Diffeomorphic add-on.
<br/>
<br/>

{{< step text="1. Install DAZ Studio" >}} 
   DAZ Studio is available for free {{< link href="https://www.daz3d.com/get_studio/" >}}on their website{{< /link >}}, though you will need to make an account. Next, it will download a standalone application called **DazCentral**.
   <br/>
   <br/>
   When you open DazCentral for the first time, it suggests you install Daz Studio via a welcome popup. The "Daz Studio" box is pre-checked so proceed and install it.
   <br/>
   <br/>
   After that, when opening DazCentral, you should have <span class="tooltip" data-tooltip-content="#tooltip-daz-central">Daz Studio  available</span> (you might need to press the Reload button in the top right corner).
{{< /step >}}


{{< step text="2. Set up a custom Library folder in Daz Studio" >}}
   My suggestion would be that you keep all your Daz assets in a folder separate from the main Daz installation. 
   Let's say you make an empty folder called `C:\3D\MyDazLibrary`
   <br/>
   <br/>
   Open up the Daz Studio and go to the <span class="tooltip" data-tooltip-content="#tooltip-daz-content-library">Content Library panel</span> on the left side. 
   Right click **DAZ Studio formats** at the top and choose **Add a base directory** and then navigate to your new (and empty) Daz library folder. 
{{< /step >}}


{{< step text="3. Install and set up the Diffeomorphic DAZ importer" >}}
   Go to the {{< link href="https://diffeomorphic.blogspot.com/p/daz-importer-version-16.html" >}}diffeomorphic{{< /link >}} website and download the stable version of the **DAZ Importer bundle** (the file should be called something like `import_daz_mhx_rts_v1_6_1.zip`).
   Download the zip which includes the DAZ importer, the MHX addon and some other things which you don't need. Extract the zip file, open the `to_daz_studio` folder and copy the contents to your DAZ library folder. 
   <br/>
   <br/>
   You should end up with something like this: `C:\3D\MyDazLibrary\Scripts\Diffeomorphic`
   <br/>
   <br/>
   Next, open up DAZ Studio and in the Content Library <span class="tooltip" data-tooltip-content="#tooltip-daz-scripts">navigate to the Scripts folder</span> and under **Diffeomorphic** double click the **Setup Menus** option. You should get a confirmation and a <span class="tooltip" data-tooltip-content="#tooltip-daz-export-menu">new entry in the File menu</span>: **File → Export** to Blender (no need to click it now).  
   <br/>
   In addition to the **Setup Menus** option you will always have the **Save paths** one. Double click it and save the output .JSON file (it doesn't matter where). This will be needed in the following step.
{{< /step >}}


{{< step text="4. Install and set up the MHX rig add-on" >}}
   Open Blender and go to **Edit → Preferences... → Add-ons** then click **Install...** and select the zip file downloaded in the previous step (such as `import_daz_mhx_rts_v1_6_1.zip`).
   <br/>
   <br/>
   Then, search for `mhx` and enable the **Rigging: MHX Runtime system** add-on and then search for `daz`  and enable the **Import-Export: DAZ importer** add-on  <span class="tooltip" data-tooltip-content="#tooltip-blender-addons">as displayed here</span>.
   <br/>
   <br/>
   You should get a <span class="tooltip" data-tooltip-content="#tooltip-daz-panels">DAZ Importer and MHX panel</span>. From the **Daz Importer** panel click **Global settings** and then click **Load Root Paths** and select the .JSON file from the previous step. Finally, click **OK** to close the Global settings window.

{{< /step >}}



