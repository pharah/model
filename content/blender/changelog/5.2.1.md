---
title: "version 1.0"
draft: false
weight: 391
version: "5.2.1"
releaseDate: "07 JAN 2023"
beforeChecker: false
---

**Updates:**

  + **Added Overwatch 2 skin** - This includes the default armor, new hairdo and teeth. The armor comes with two additional color variations. ([preview](https://i.imgur.com/WPVK9Lb.png))
  
  + **Added zero-suit** - This complements the armor well but works as a standalone item or combined with other outfits. It comes in 3 material variations and includes torn options, for lewd. ([preview](https://i.imgur.com/9dOJZW8.png))

  + **Improved poses library** - I re-did the thumbnails for all the poses to color code them and group them visually. I also added a bunch of new poses, including better facial poses. ([preview](https://i.imgur.com/ghpVxVZ.jpg))

  
  + **Changed the knee-high boots** - the older ones didn't work so well with some of the outfits, so I ported the boots from the v4 model as a replacement ([preview](https://i.imgur.com/YwdPRdV.jpg))

  + **Redid the face options** - I added the variations as overlays instead of separate textures. Redid all the options and added some new ones such as smudged lipstick. ([preview](https://i.imgur.com/42I9cDT.png))
  
  + **Added color variations** - the sports lingerie now comes with Blacked/Bleached options as well. The bodysuits also have color variations so that they fit better with the armor

  + Changed diffuse textures from JPG tp PNG and adjusted the face ones (suggested by [@blankpins](https://twitter.com/blankpins))
  

**Fixes:**
  
  + Fixed some busted vertices for the pregnant shapekey (reported by imh0ll0w)

  + It's now possible to use just the bottom tanlines (reported by Klopparberg)

  + Fixed issues with some materials not looking properly (reported by [@GRVTY3D](https://twitter.com/GRVTY3D)) 
