---
title: "version 1.0"
draft: false
weight: 396
version: "5.1.26"
releaseDate: "01 JUN 2022"
beforeChecker: false
---

  * Fixed some hairdos and outfits not following the rig (thanks to [@blankpins](https://twitter.com/blankpins))