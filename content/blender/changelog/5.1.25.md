---
title: "version 1.0"
draft: false
weight: 397
version: "5.1.25"
releaseDate: "01 JUN 2022"
beforeChecker: false
---

**Updates:**

  * Added [sports hoodie](https://i.imgur.com/9sRxkQo.png)
  * Added [sports thigh highs](https://i.imgur.com/bQd09M7.png)
  * Replaced knee-high boots with [less boring looking ones](https://i.imgur.com/slqMvC8.png)
  * Added [sandals](https://i.imgur.com/UJlpKBV.png)
  * Added strap-on [sandals](https://i.imgur.com/N3Lighd.png)
  * Added UI controls to toggle rig layers and check for the presence of the MHX addon (needed for FK controls)

**Fixes:**

  * Changed the default nodes in the compositor (rendering will no longer show a "white image")
  * Fixed body options not having the anubis tattoo (thanks to [@ShadowBoxer3D](https://twitter.com/Boxer3D))
  * Fixed some hairdos' vertices not following the rig (thanks to [@blankpins](https://twitter.com/blankpins)
  * Fixed tongue material being too dark