---
title: Pharah v5 for Blender v3
description: Free 3D model of Pharah from Overwatch for Blender v3
model: blender
download: https://smutba.se/project/431/
downloadAlt: https://mega.nz/folder/XrhwkICD#cL325E3vvIDoU9BB_6qszg
updateDate: 05 Jun
version: 5.3.45
supportedVersions: ["5.3.45", "5.2.2", "5.2.1", "5.1.35", "5.1.33", "5.1.28", "5.1.27", "5.1.26", "5.1.25", "5.1.23"]
themeColor: "#827ea3"
---
