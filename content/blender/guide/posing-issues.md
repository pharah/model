---
title: "Posing issues"
date: "2017-11-12T00:00:00Z"
draft: false
weight: 60
model: blender
slug: "posing-issues" 
thumb: true
---

Before the v3.5 update  there were two problems when posing the elbows and knees.

* When posing the elbow, <span class="tooltip" data-tooltip-content="#tooltip-posing-problem">the palm would be moving</span>

* When using some of the poses from the library, the elbows / knees posing would break

For the first problem, the initial solution was to enable **Arm Stretchiness** in the MHX addon. 
This seemed to solve the issue and the palm would no longer move when posing the breast. 
However, this would also mean that in some situations the forearms would get stretched or squashed, looking messed up. 
The solution I ended up with is removing the stretchiness and disabling the **Limits** under **IK and FK limits** option in the MHX addon.

The second problem would happen after using the poses from the Poses library and the end result would be the knees bones being rotated the wrong way or the elbow bones being offset. The easiest way to know if this happened was to select all the bones, reset their position and scale, and then instead of the body being in the proper A pose <span class="tooltip" data-tooltip-content="#tooltip-posing-problem-2">the elbows would still be bent</span>. 

Apparently the problem here was how the poses were imported, so I re-imported almost all of them and made sure the bones were rotated properly. Despite that, if somehow this still happens or if I missed this problem on one of the existing poses, the solution lies again in the MHX addon. In this case, pressing the **Enforce limits** button in the MHX addon will fix the issue.
