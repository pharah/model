---
title: "importing poses"
date: "2017-11-12T00:00:00Z"
draft: false
weight: 20
model: blender
slug: "import-poses" 
thumb: true
---

Another benefit of using the Diffeomorphic plugin is that it makes it easy to import Poses from DAZ Studio
<br/>

Once you're familiar with the process and have the temp .blend file saved you should be able to import poses in under one minute minutes.

<a class="btn btn-primary" target="_self" onclick="track(this, 'blender user guide', 'view section', 'import poses');" href="/blender/daz-to-blender/#export-daz-poses">Read the guide</a>

