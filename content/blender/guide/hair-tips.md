---
title: "weird dark hair tips"
date: "2017-11-12T00:00:00Z"
draft: false
weight: 50
model: blender
slug: "hair-tips" 
thumb: true
---

In Cycles, you might get some weird looking dark hair tips, especially noticeable in the blonde Karla hairdo. 
To reduce these artefacts you should change the following setting: 
**Scene → Light paths → Max bounces** and increase  the <span class="tooltip" data-tooltip-content="#tooltip-transparent-bounces">Transparent</span> value to around 24 or more. 
