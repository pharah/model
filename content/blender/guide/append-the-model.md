---
title: "importing the model to a separate scene"
date: "2017-11-12T00:00:00Z"
draft: false
weight: 5
model: blender
slug: "import-model" 
thumb: true
---

To import this model to a separate scene you will need to append the entire collection.
<br/>
In your new scene go to **File → Append ...→ Pharah v5.x.xx.blend → Collection → Pharah v5** 

You might get some <span class="tooltip" data-tooltip-content="#tooltip-empty-shapes">wire shapes added to the Scene</span>. Select those and hide or delete them.

Lastly, save the scene and then re-open it. This will add the custom UI (Pharah v5 panel in the viewport).







