---
title: "importing outfits"
date: "2017-11-12T00:00:00Z"
draft: false
weight: 10
model: blender
slug: "import-outfits" 
thumb: true
---

Being able to easily import outfits was the main reason to make this new model. 
<br/>
I've set up a straightforward guide that will take you through the process of setting up DAZ Studio and the Diffeomorphic add-on to easily export outfits from DAZ and import them into Blender. 

Once you're familiar with the process you should be able to import outfits and have them ready to go (rigged, shaded and working with bodymorphs) in under 5 minutes.

<a class="btn btn-primary" target="_self" onclick="track(this, 'blender user guide', 'view section', 'import outfits');" href="/blender/daz-to-blender/">Read the guide</a>


