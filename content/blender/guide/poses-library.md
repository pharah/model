---
title: "pose library"
date: "2017-11-12T00:00:00Z"
draft: false
weight: 30
model: blender
slug: "pose-library" 
thumb: true
---


This model comes with a collection of poses for the full body, hands and facial expressions. These are available within Blender 3.0 <span class="tooltip" data-tooltip-content="#tooltip-poses-library">Asset Browser</span>.
<br/>
<br/>
To make use of them you will first need to enable the **Pose Library** add-on that comes with Blender. To do that and see how to use it you can {{< link href="https://youtu.be/51rTbKTNj6U?t=431" >}}follow this short guide{{< /link >}} which summarizes it really well.
