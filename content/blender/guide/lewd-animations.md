---
title: "Jiggle physics"
date: "2017-11-12T00:00:00Z"
draft: false
weight: 40
model: blender
slug: "lewd-animations" 
thumb: true
---

The model comes with bones to controls the breasts, butt and vagina which an be used for posing or animating. 

The bones are available under **Posing → Lewd** in the Pharah panel.

To add jiggle physics to the model you can {{< link href="https://vimeo.com/906521374" >}}follow the steps in this video{{< /link >}}:

 * Download the ZIP file of the {{< link href="https://github.com/shteeve3d/blender-wiggle-2" >}}Wiggle 2 addon{{< /link >}}:

 * Extract the content of the archive

 * In Blender, go to **Edit → Preferences → Addon** and install the `.py` file you extracted from the ZIP **(*don't try to install the ZIP itself, it did not work for me*)**

 * Select the Pharah rig, and go into **Pose mode** and select the bones you want to animate
 
 * In the 3D viewport, in the **Animation panel** you should see a new sub-panel called **Wiggle 2**

 * Simply enable the **Bone head** and **Bone tail** options for the selected bones

That's it. Now when you move the body you should have jiggle physics. 

 