function track(elem, category, action, name) {
  if ($(elem).attr("aria-expanded")) {
    _paq.push(["trackEvent", category, action, name]);
  }
  if (!$(elem).attr("aria-expanded")) {
    _paq.push(["trackEvent", category, action, name]);
  }
}

function sliderUpdateAuthor(slide, info) {
  var $s = slide.find(".slick-active");
  info.html(
    'by <a href="' +
      $s.data("url") +
      '" target="_blank">' +
      $s.data("author") +
      "</a>"
  );
}

function toggleMenu() {
  $("#navbar").toggleClass("open");
}

function closeModal() {
  $("#version-checker").attr("aria-hidden", "true");
  $("body").removeClass("modal-open");
}

$(document).ready(function () {
  //-----------------------------------------------------------------[ version checker ]

  const params = new URLSearchParams(window.location.search);
  const userVersion = params.get("ver");

  if (userVersion) {
    $("#version-checker").attr("aria-hidden", "false");
    $("body").addClass("modal-open");
    const foundVersionIndex = SUPPORTED_VERSIONS.indexOf(userVersion);

    if (foundVersionIndex >= 0) {
      if (SUPPORTED_VERSIONS[0] === userVersion) {
        $("#version-current").attr("aria-hidden", "false");
      } else {
        $("#version-old").attr("aria-hidden", "false");
        $("#user-version").html(userVersion);

        const newVersions = SUPPORTED_VERSIONS.slice(0, foundVersionIndex);
        // console.log("userVersion=", userVersion)
        // console.log("newVersions=", newVersions)
        for (var v in newVersions) {
          const section = document.getElementById("version-" + newVersions[v]);
          if (section) {
            $(section).attr("aria-hidden", "false");
          }
        }
      }
    } else {
      $("#version-unsupported").attr("aria-hidden", "false");
    }
  }

  //-----------------------------------------------------------------[ close mobile menu on click ]

  $("#navbar .navbar-nav .nav-link ").each(function () {
    $(this).click(function () {
      toggleMenu();
    });
  });

  //-----------------------------------------------------------------[ alt version ]

  const downloadParam = params.get("download");
  if (downloadParam == "alt") {
    $("a.download-link").each(function () {
      $(this).attr("href", DOWNLOAD_ALT);
    });
  }

  //-----------------------------------------------------------------[ tooltips ]
  $("#examples-slider").slick({
    lazyLoad: "ondemand",
    slidesToShow: 3,
    slidesToScroll: 3,
    arrows: false,
    dots: true,
    infinite: true,
    speed: 1000,
    autoplay: true,
    autoplaySpeed: 5000,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  });

  //-----------------------------------------------------------------[ sliders ]
  var $sliderExamplesMine = $("#slider-examples-mine");
  var $sliderExamplesMineInfo = $("#example-author-mine");
  var $sliderExamplesOthers = $("#slider-examples-others");
  var $sliderExamplesOthersInfo = $("#example-author-others");

  $sliderExamplesMine.slick({
    arrows: false,
    dots: true,
    infinite: true,
    speed: 750,
    autoplay: true,
    autoplaySpeed: 5000,
    appendDots: $("#slider-buttons-mine"),
    customPaging: function (slider, pageIndex) {
      return $("<button></button").text(myWorks[pageIndex].toUpperCase());
    },
  });

  $sliderExamplesOthers.slick({
    lazyLoad: "ondemand",
    arrows: false,
    dots: true,
    infinite: true,
    speed: 750,
    autoplay: true,
    autoplaySpeed: 5000,
    appendDots: $("#slider-buttons-others"),
    customPaging: function (slider, pageIndex) {
      return $("<button></button").text(
        "by " + othersWorks[pageIndex].toUpperCase()
      );
    },
  });
  sliderUpdateAuthor($sliderExamplesMine, $sliderExamplesMineInfo);
  sliderUpdateAuthor($sliderExamplesOthers, $sliderExamplesOthersInfo);

  //-----------------------------------------------------------------[ tooltips ]

  $(".tooltip").tooltipster({
    theme: "tooltipster-light",
  });

  //-----------------------------------------------------------------[ change navbar on scroll ]
  var $body = $("body");
  $body.scrollspy({ target: "#navbar" });
});
