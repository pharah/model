// change URL on scroll based on <section> items with IDs

$(document).ready(function () {
  var timeout,
    curSection,
    prevSection,
    sections = [];
  $("section").each(function () {
    if ($(this).attr("id")) sections.push($(this));
  });
  $(window).scroll(function () {
    var scrolled = $(document).scrollTop();
    for (var i = 0; i < sections.length; i++) {
      if (sections[i].offset().top >= scrolled) {
        if (i === 0) curSection = sections[0];
        else {
          curSection = sections[i - 1];
          break;
        }
      }
    }
    if (sections[sections.length - 1].offset().top < scrolled)
      curSection = sections[sections.length - 1];
    if (!prevSection || curSection != prevSection) {
      history.pushState(null, null, "#" + curSection.attr("id"));
      prevSection = curSection;
    }
  });
});
