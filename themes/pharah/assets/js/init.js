function loadPreview(imageId, componentName) {
	
	var img = document.getElementById("preview-" + imageId);
	img.src = "./assets/previews/" + componentName + ".png";

	try {
		ga('send', 'event',  'component-clicked', componentName);
	} catch (e) {}
}